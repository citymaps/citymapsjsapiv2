/**
 *
 * Citymaps JS API v2 library
 * Based on Leaflet 1.2
 *
 * @author bgundersen
 * @since 6/2017
 *
 */


'use strict';

var leaflet = require('leaflet/dist/leaflet-src');
window.Leaflet = leaflet;

require('./src/citymaps');

// Leaflet extensions
require('./src/leaflet/Leaflet.Marker.Label');
require('./src/leaflet/Leaflet.Map.Label');
require('./src/leaflet/Leaflet.TileLayer.FixedZoomAnim');
window.Leaflet.DirectionsRenderer = require('./src/leaflet/routing/Leaflet.DirectionsRenderer');
window.Leaflet.DirectionsService = require('./src/leaflet/routing/Leaflet.DirectionsService');

require('leaflet.markercluster');

typeof define === 'function' && define.amd ?
  define('maps/lib/citymaps', [], function() {
    return leaflet;
  })
: false;
