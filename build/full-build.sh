#!/bin/bash -xe

full_path="$(realpath $0)"
dir="$(dirname $full_path)"
cd $dir/..

npm install
./build.sh
