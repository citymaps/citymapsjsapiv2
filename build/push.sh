#!/bin/bash -e
#
# refactored from build.sh. Will still be called from there
#

full_path="$(realpath $0)"
dir="$(dirname $full_path)"
cd $dir/..

if [ "$1" != "" ]; then
    version=$1
else
    version=v$prefix$(jq -r .version ./package.json)
fi

git add .
git commit -m "Auto-building version $version"

echo "Pushing"
git push
git tag -a $version -m "version $version"
git push origin --tags
