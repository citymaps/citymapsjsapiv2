#!/bin/bash -xe

full_path="$(realpath $0)"
dir="$(dirname $full_path)"
cd $dir/

docker build -t npm-js-api-builder -f Dockerfile.build .

cd ..

docker run -e AWS_ACCESS_KEY_ID="$AWS_ACCESS_KEY_ID" -e AWS_SECRET_ACCESS_KEY="$AWS_SECRET_ACCESS_KEY" -v $(pwd):/citymapsjsapiv2 npm-js-api-builder build/js-s3-deploy.sh
