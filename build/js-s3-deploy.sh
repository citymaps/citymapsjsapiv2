#!/bin/bash -xe
export PATH=$PATH:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin
cd "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
set -eu

#if [ $# -eq 0 ]
#  then
#    echo "No git tag name supplied"
#    exit 1
#fi
#gittag="$1"
lasttag=$(git tag --points-at HEAD)
uuid=$(/usr/bin/uuidgen)
who am i
aws --version
echo $lasttag

if [[ "$lasttag" != "" ]]; then
  mkdir -p /tmp/$uuid/$lasttag
  cd /tmp/$uuid/$lasttag
  git clone --branch $lasttag https://citymapsanton@bitbucket.org/citymaps/citymapsjsapiv2.git
  aws s3 sync --acl public-read /tmp/$uuid/$lasttag/citymapsjsapiv2/dist/ s3://cm-ta-media/lib/$lasttag
  rm -Rf /tmp/$uuid
else
  echo "not attempting to upload because no tag points at HEAD"
fi
