#!/bin/bash -e

full_path="$(realpath $0)"
dir="$(dirname $full_path)"
cd $dir/..

# Uses 'jq' command-line tool - brew install jq
# Note that the -r parameter returns a raw string, rather than with quotes
version=v$prefix$(jq -r .version ./package.json)

echo "Building $version"
browserify index.js -t [ babelify --presets [ es2015 ] ] -o ./dist/citymaps.js --igv __filename,__dirname,process,Buffer,global,define

echo "Compiling CSS"
lessc ./index.less ./dist/citymaps.css

if [ "$1" != "--deploy" ]; then
   echo "not pushing, please pass --deploy if you want to push as well"
else
   echo "Building $version minified"
   browserify index.js -t [ babelify --presets [ es2015 ] ] | uglifyjs > ./dist/citymaps.min.js  
   $dir/push.sh $version
fi
