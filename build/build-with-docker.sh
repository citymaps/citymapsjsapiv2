#!/bin/bash -xe

full_path="$(realpath $0)"
dir="$(dirname $full_path)"
cd $dir/

docker build -t npm-js-api-builder -f Dockerfile.build .

cd ..

docker run -v $(pwd):/citymapsjsapiv2 npm-js-api-builder build/full-build.sh
