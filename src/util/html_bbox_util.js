'use strict';

let isMSIE8 = !('getComputedStyle' in window && typeof window.getComputedStyle === 'function');

module.exports = {
  // Returns a plain array with the relative dimensions of a L.Icon, based
  //   on the computed values from iconSize and iconAnchor.
  getElementBox: function (el) {

    if (isMSIE8) {
      // Fallback for MSIE8, will most probably fail on edge cases
      return {
        minX: 0,
        minY: 0,
        maxX: el.offsetWidth,
        maxY: el.offsetHeight
      };
    }

    let styles = window.getComputedStyle(el);

    // getComputedStyle() should return values already in pixels, so using parseInt()
    //   is not as much as a hack as it seems to be.
    return {
      minX: parseInt(styles.marginLeft),
      minY: parseInt(styles.marginTop),
      maxX: parseInt(styles.marginLeft) + parseInt(styles.width),
      maxY: parseInt(styles.marginTop)  + parseInt(styles.height)
    };
  },

  // Much like getElementBox, but works for positioned HTML elements, based on offsetWidth/offsetHeight.
  getRelativeElementBoxes: function(els,baseBox) {
    let boxes = [];
    for (let i = 0; i < els.length; i++) {
      let el = els[i];
      let box = {
        minX: el.offsetLeft,
        minY: el.offsetTop,
        maxX: el.offsetLeft + el.offsetWidth,
        maxY: el.offsetTop + el.offsetHeight
      };
      box = this._offsetBoxes(box, baseBox);
      boxes.push( box );

      if (el.children.length) {
        let parentBox = baseBox;
        if (!isMSIE8) {
          let positionStyle = window.getComputedStyle(el).position;
          if (positionStyle === 'absolute' || positionStyle === 'relative') {
            parentBox = box;
          }
        }
        boxes = boxes.concat( this.getRelativeElementBoxes(el.children, parentBox) );
      }
    }
    return boxes;
  },

  // Adds the coordinate of the layer (in pixels / map canvas units) to each box coordinate.
  positionBoxes: function(offset, boxes, margin) {
    let newBoxes = [];	// Must be careful to not overwrite references to the original ones.
    for (let i = 0; i < boxes.length; i++) {
      newBoxes.push( this.positionBox( offset, boxes[i], margin ) );
    }
    return newBoxes;
  },

  positionBox: function(offset, box, margin) {
    margin = margin || 0;

    return {
      minX: box.minX + offset.x - margin,
      minY: box.minY + offset.y - margin,
      maxX: box.maxX + offset.x + margin,
      maxY: box.maxY + offset.y + margin,
    };
  },

  _offsetBoxes: function(a,b){
    return {
      minX: a.minX + b.minX,
      minY: a.minY + b.minY,
      maxX: a.maxX + b.minX,
      maxY: a.maxY + b.minY
    };
  },
};