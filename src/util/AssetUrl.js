const VERSION = require('../../package.json').version;

const LOCAL_PATH = "/CitymapsJSApi/dist/";
const REMOTE_PATH = "https://ta-media.citymaps.io/lib/v"+VERSION+"/";
const is_local = (location.hostname === "localhost" || location.hostname === "127.0.0.1");

const BASE_PATH = is_local ? LOCAL_PATH : REMOTE_PATH;

module.exports = function(asset) {
  return BASE_PATH + asset;
};