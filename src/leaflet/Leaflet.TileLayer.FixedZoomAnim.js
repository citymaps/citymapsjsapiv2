
Leaflet.TileLayer.mergeOptions({
  fixZoomAnim: false
});

Leaflet.TileLayer.include({
  _originalSetZoomTransform: Leaflet.TileLayer.prototype._setZoomTransform,

  _setZoomTransform: function(level, center, zoom) {
    // The fix is to use map zoom when it's fractional.
    if (this.options.fixZoomAnim && !Number.isInteger(this._map.getZoom())) {
      zoom = this._map.getZoom();
    }
    return this._originalSetZoomTransform.call(this, level, center, zoom);
  }
});