let RelativeAnchorIcon = Leaflet.Icon.extend({
  options: {
    iconRelativeAnchor: [0.5, 1.0],
    shadowRelativeAnchor: [0.5, 1.0]
  },
  createIcon: function(oldEl) {
    let src = this._getIconUrl('icon');

    if (!src) {
      throw new Error('iconUrl not set in Icon options (see the docs).');
    }

    let img = oldEl || new Image();
    img.width = img.height = 0;

    img.onload = function() {
      if (!this.options.iconSize) {
        this.options.iconSize = [img.naturalWidth, img.naturalHeight];
      }
      if (!this.options.iconAnchor) {
        let anchorX = this.options.iconSize[0] * this.options.iconRelativeAnchor[0];
        let anchorY = this.options.iconSize[1] * this.options.iconRelativeAnchor[1];
        this.options.iconAnchor = [anchorX, anchorY];
      }
      let className = img.className;
      this._setIconStyles(img, 'icon');
      img.classList.add(...className.split(' '));
    }.bind(this);

    img.src = src;
    return img;
  },
  createShadow: function(oldEl) {
    let src = this._getIconUrl('shadow');
    if (!src) {
      return null;
    }

    let img = oldEl || new Image();
    img.width = img.height = 0;

    img.onload = function() {
      if (!this.options.shadowSize) {
        this.options.shadowSize = [img.naturalWidth, img.naturalHeight];
      }
      if (!this.options.shadowAnchor) {
        let anchorX = this.options.shadowSize[0] * this.options.shadowRelativeAnchor[0];
        let anchorY = this.options.shadowSize[1] * this.options.shadowRelativeAnchor[1];
        this.options.shadowAnchor = [anchorX, anchorY];
      }
      let className = img.className;
      this._setIconStyles(img, 'shadow');
      img.classList.add(className.split(' '));
    }.bind(this);

    img.src = src;
    return img;
  }
});

module.exports.relativeAnchorIcon = function(options) {
  return new RelativeAnchorIcon(options);
};
