/**
 * This class adds html label support to markers.
 */

let LeafletLabel = require("./Leaflet.Label");

// Add in an option to icon that is used to set where the label anchor is
Leaflet.Icon.Default.mergeOptions({
  labelOffset: new Leaflet.Point(3, 3)
});

// Have to do this since Leaflet is loaded before this plugin and initializes
// Leaflet.Marker.options.icon therefore missing our mixin above.
Leaflet.Marker.mergeOptions({
  icon: new Leaflet.Icon.Default()
});

Leaflet.Marker.include({
  _labelShowing: false,

  _originalUpdateZIndex: Leaflet.Marker.prototype._updateZIndex,
  _updateZIndex: function (offset) {
    let zIndex = this._zIndex + offset;

    this._originalUpdateZIndex.call(this, offset);

    if (this.label) {
      this.label.updateZIndex(zIndex);
    }
  },

  _originalUpdateOpacity: Leaflet.Marker.prototype._updateOpacity,
  _updateOpacity: function () {
    this._originalUpdateOpacity.call(this);
    if (this.label) {
      this.label.setOpacity(this.options.opacity);
    }
  },

  _originalSetIcon: Leaflet.Marker.prototype.setIcon,
  setIcon: function(icon) {
    this._originalSetIcon.call(this, icon);
    if (this.label) {
      this.label._updatePosition();
    }
  },

  showLabel: function () {
    this._labelShowing = true;
    if (this.label && this._map) {
      this.label.setLatLng(this._latlng);
      this._map.showLabel(this.label);
    }

    return this;
  },

  hideLabel: function () {
    this._labelShowing = false;
    if (this.label) {
      this.label.close();
    }
    return this;
  },
  
  bindLabel: function (content, options) {
    if (!this.label) {
      this
        .on('remove', this._onRemoved, this)
        .on('move', this._moveLabel, this)
        .on('add', this._onAdded, this);
      this._hasLabelHandlers = true;
    }

    this.label = new LeafletLabel.label(options, this)
      .setContent(content);

    return this;
  },

  _onAdded: function() {
    if (this._labelShowing) {
      this.showLabel();
    }
  },

  _onRemoved: function() {
    if (this.label) {
      this.label.close();
    }
  },

  updateLabelDirection: function(direction) {
    if (this.label) {
      this.label.setDirection(direction);
    }
    return this;
  },

  unbindLabel: function () {
    if (this.label) {
      this.hideLabel();

      this.label = null;

      if (this._hasLabelHandlers) {
        this
          .off('remove', this._onRemoved, this)
          .off('move', this._moveLabel, this)
          .off('add', this._onAdded, this);
      }
      this._hasLabelHandlers = false;
    }
    return this;
  },

  updateLabelContent: function (content) {
    if (this.label) {
      this.label.setContent(content);
    }
    return this;
  },

  getLabel: function () {
    return this.label;
  },

  _moveLabel: function (e) {
    this.label.setLatLng(e.latlng);
  }
});