/**
 * This class adds the ability to disable resize debouncing in leaflet.
 */

Leaflet.Map.mergeOptions({
  debounceResize: true
});

Leaflet.Map.include({
  _originalResize: Leaflet.Map.prototype._onResize,
  _onResize: function() {
    if (this.options.debounceResize) {
      this._originalResize.call(this);
    } else {
      this.invalidateSize({debounceMoveend: true});
    }
  }
});