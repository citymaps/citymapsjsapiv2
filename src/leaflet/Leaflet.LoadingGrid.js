/**
 *  Create a Canvas as ImageOverlay to draw the Lat/Lon Graticule
 */

let LoadingGrid = Leaflet.Layer.extend({
  includes: Leaflet.Evented,
  options: {
    opacity: 1,
    tileEdgeWidth: 2,
    innerLineWidth: 1,
    color: '#fff',
    tileSize: 256,
  },
  _needsToRender: true,

  initialize: function (options) {
   Leaflet.setOptions(this, options);
  },

  setBaseTileLayer: function(layer) {

    this._needsToRender = layer._loading;

    let self = this;

    layer.on('tileloadstart', function(){
      self._needsToRender = true;
    });

    layer.on('load', function(){
      self._needsToRender = false;
    });
  },

  onAdd: function (map) {
    this._map = map;

    if (!this._container) {
      this._initCanvas();
    }

    this._pane = map.getPane("loadingGrid");
    if (!this._pane) {
      this._pane = map.createPane("loadingGrid");
      this._pane.style.zIndex = 0;
    }
    this._pane.appendChild(this._container);

    map.on('viewreset', this._reset, this);
    map.on('move', this._move, this);
    map.on('moveend', this._reset, this);

    if (this.options.zoomAnimation && map.options.zoomAnimation && Leaflet.Browser.any3d) {
      map.on('zoomanim', this._animateZoom, this);
    }

    this._reset();
  },

  onRemove: function (map) {
    this._pane.removeChild(this._container);
    this._pane = null;

    map.off('viewreset', this._reset, this);
    map.off('move', this._move, this);
    map.off('moveend', this._reset, this);

    if (this.options.zoomAnimation && map.options.zoomAnimation) {
      map.off('zoomanim', this._animateZoom, this);
    }
  },

  addTo: function (map) {
    map.addLayer(this);
    return this;
  },

  setOpacity: function (opacity) {
    this.options.opacity = opacity;
    this._updateOpacity();
    return this;
  },

  bringToFront: function () {
    if (this._canvas) {
      this._pane.appendChild(this._canvas);
    }
    return this;
  },

  bringToBack: function () {
    if (this._canvas) {
      this._pane.insertBefore(this._canvas, this._pane.firstChild);
    }
    return this;
  },

  getAttribution: function () {
    return this.options.attribution;
  },

  _initCanvas: function () {
    this._container = Leaflet.DomUtil.create('div', 'leaflet-image-layer');

    this._canvas = Leaflet.DomUtil.create('canvas', '');

    if (this.options.zoomAnimation && this._map.options.zoomAnimation && Leaflet.Browser.any3d) {
      Leaflet.DomUtil.addClass(this._canvas, 'leaflet-zoom-animated');
    } else {
      Leaflet.DomUtil.addClass(this._canvas, 'leaflet-zoom-hide');
    }

    this._updateOpacity();

    this._container.appendChild(this._canvas);

    Leaflet.extend(this._canvas, {
      onselectstart: Leaflet.Util.falseFn,
      onmousemove: Leaflet.Util.falseFn,
      onload: Leaflet.bind(this._onCanvasLoad, this)
    });
  },

  _animateZoom: function (e) {
    this._needsToRender = true;
    this._reset();
  },

  _move: function () {
    let container = this._container,
      lt = this._map.containerPointToLayerPoint([0, 0]);
    Leaflet.DomUtil.setPosition(container, lt);
    this.__draw(true);
  },

  _reset: function () {
    let container = this._container,
      canvas = this._canvas,
      size = this._map.getSize(),
      lt = this._map.containerPointToLayerPoint([0, 0]);

    Leaflet.DomUtil.setPosition(container, lt);

    container.style.width = size.x + 'px';
    container.style.height = size.y + 'px';

    canvas.width  = size.x;
    canvas.height = size.y;
    canvas.style.width  = size.x + 'px';
    canvas.style.height = size.y + 'px';

    this.__draw(true);
  },

  _onCanvasLoad: function () {
    this.fire('load');
  },

  _updateOpacity: function () {
    Leaflet.DomUtil.setOpacity(this._canvas, this.options.opacity);
  },

  __draw: function() {
    let canvas = this._canvas,
        map = this._map;

    if (!Leaflet.Browser.canvas || !map || !this._needsToRender) {
      return;
    }

    let ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.strokeStyle = this.options.color;

    function __draw_lat_line(self, pixelX, pixelBounds) {

      let canvasStart = self._pixelCoordToCanvasPoint({x: pixelX, y: pixelBounds.min.y});
      let canvasEnd = self._pixelCoordToCanvasPoint({x: pixelX, y: pixelBounds.max.y});

      ctx.moveTo(canvasStart.x, canvasStart.y);
      ctx.lineTo(canvasEnd.x, canvasEnd.y);
    }

    function __draw_lon_line(self, pixelY, pixelBounds) {

      let canvasStart = self._pixelCoordToCanvasPoint({x: pixelBounds.min.x, y: pixelY});
      let canvasEnd = self._pixelCoordToCanvasPoint({x: pixelBounds.max.x, y: pixelY});

      ctx.moveTo(canvasStart.x, canvasStart.y);
      ctx.lineTo(canvasEnd.x, canvasEnd.y);
    }

    let nearestMultiple = function (num, interval) {
      return Math.ceil(num / interval) * interval;
    };

    let drawInterval = function(self, interval) {
      let pixelBounds = map.getPixelBounds();
      let startX = nearestMultiple(pixelBounds.min.x, interval);
      let startY = nearestMultiple(pixelBounds.min.y, interval);

      ctx.beginPath();

      for (let i = startX; i <= pixelBounds.max.x; i += interval) {
        __draw_lat_line(self, i, pixelBounds);
      }

      for (let i = startY; i <= pixelBounds.max.y; i += interval) {
        __draw_lon_line(self, i, pixelBounds);
      }
      ctx.stroke();
    };

    ctx.lineWidth = this.options.tileEdgeWidth;
    drawInterval(this, this.options.tileSize);

    ctx.lineWidth = this.options.innerLineWidth;
    drawInterval(this, this.options.tileSize / 10);
  },

  _pixelCoordToCanvasPoint: function(pixelCoord) {
    let map = this._map;
    return Leaflet.point(pixelCoord)
      .subtract(map.getPixelOrigin())
      .add(map._getMapPanePos());
  }
});

module.exports.loadingGrid = function (options) {
  return new LoadingGrid(options);
};