/**
 DirectionsRenderer
 */

let polylineUtil = require('polyline-encoded');

let DirectionsRenderer = Leaflet.Class.extend({

  options: {
    directions: null,

    map: null,

    polylineOptions: {},

    routeIndex: 0,

    suppressPolylines: false
  },

  featureGroup: null,

  initialize: function(options) {
    L.setOptions(this, options);

    this.featureGroup = new L.featureGroup();
    if (this.options.map) {
      this.featureGroup.addTo(this.options.map);
    }

    this._draw();
  },

  setMap: function(map) {
    this.options.map = map;

    if (this.options.map) {
      this.featureGroup.addTo(this.options.map);
    }

    this._draw()
  },

  setDirections: function(result) {
    this.options.directions = result;

    this._draw();
  },

  setOptions: function(options) {
    L.setOptions(this, options);

    this._draw();
  },

  setRouteIndex: function(index) {
    this.options.routeIndex = index;

    this._draw();
  },

  getMap: function() {
    return map;
  },

  getDirection() {
    return this.directions;
  },

  getOptions() {
    return this.options;
  },

  getRouteIndex() {
    return this.routeIndex;
  },

  _draw: function() {
    if (!this.options.map) {
      return;
    }

    this.featureGroup.clearLayers();

    if (this.options.directions && !this.options.suppressPolylines) {
      let route = this.options.directions.routes[this.options.routeIndex];
      let polyline = L.polyline(polylineUtil.decode(route.geometry, 5), this.options.polylineOptions);
      this.featureGroup.addLayer(polyline);
    }
  }
});

module.exports = DirectionsRenderer;