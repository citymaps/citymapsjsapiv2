/**
 *  Directions service
 */

import superagent from 'superagent';

let apiBaseURL = "https://route.citymaps.io";

let apiEndpointFormat = "/route/v1/{travelMode}/{coordinates}.json";

let DirectionsService = Leaflet.Class.extend({

  statics: {
    Code: {
      Ok: "Ok",
      InvalidUrl: "InvalidUrl",
      InvalidService: "InvalidService",
      InvalidVersion: "InvalidVersion",
      InvalidOptions: "InvalidOptions",
      InvalidQuery: "InvalidQuery",
      InvalidValue: "InvalidValue",
      NoSegment: "NoSegment",
      TooBig: "TooBig"
    },
    TravelMode: {
      Driving: "driving",
      Walking: "walking",
      Bicycle: "bicycle"
    }
  },

  defaultRequest: {
    origin: null,

    destination: null,

    waypoints: [],

    travelMode: "driving",

    transitOptions: {},

    drivingOptions: {},

    provideRouteAlternatives: false,

    avoidFerries: false,

    avoidHighways: false,

    avoidTolls: false,

    region: 'US'
  },

  route: function(request, callback) {
    request = L.Util.extend({}, this.defaultRequest, request);

    if (!request.origin || !request.destination) {
      throw new Error("Please provide a valid origin and destination");
    }

    let coordinates = [];
    coordinates.push(request.origin);
    coordinates = coordinates.concat(request.waypoints);
    coordinates.push(request.destination);

    let coordinatesString = coordinates.map(function(coord) {
      return coord.lng + "," + coord.lat;
    }).join(";");

    let apiEndpoint = apiEndpointFormat
      .replace("{travelMode}", request.travelMode)
      .replace("{coordinates}", coordinatesString);

    let params = {
      alternatives: request.provideRouteAlternatives,
      steps: true,
      geometries: 'polyline',
      overview: 'full'
    };

    let excludeList = [];
    if (request.avoidHighways) {
      excludeList.push("motorway");
    }

    if (request.avoidFerries) {
      excludeList.push("ferry");
    }

    if (request.avoidTolls) {
      excludeList.push("toll");
    }

    if (excludeList.length > 0) {
      params.exclude = excludeList.join(",");
    }

    let apiURL = apiBaseURL + apiEndpoint;

    superagent
      .get(apiURL)
      .query(params)
      .end(function(err, res) {
        if (err) { throw err; }
        callback(res.body, res.body.code);
      });
  },
});

module.exports = DirectionsService;


