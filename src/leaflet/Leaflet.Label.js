let BboxUtil = require('../util/html_bbox_util');

let LeafletLabel = Leaflet.Layer.extend({

  includes: Leaflet.Evented,

  options: {
    className: '',
    clickable: false,
    direction: 'right',
    offset: [0, 0],
    opacity: 1,
    zoomAnimation: true,
  },

  initialize: function (options, source) {
    Leaflet.setOptions(this, options);

    this._source = source;
    this._animated = Leaflet.Browser.any3d && this.options.zoomAnimation;

    this._initLayout();
  },

  onAdd: function (map) {
    this._map = map;

    this._pane = this._source instanceof Leaflet.Marker ? map._panes.markerPane : map._panes.popupPane;

    this._pane.appendChild(this._container);

    this._initInteraction();

    this._update();

    this.setOpacity(this.options.opacity);

    map.on('viewreset', this._onViewReset, this);

    if (this._animated) {
      map.on('zoomanim', this._zoomAnimation, this);
    }
  },

  onRemove: function (map) {
    this._pane.removeChild(this._container);

    map.off({
      zoomanim: this._zoomAnimation,
      viewreset: this._onViewReset
    }, this);

    this._removeInteraction();

    this._map = null;
  },

  setLatLng: function (latlng) {
    this._latlng = Leaflet.latLng(latlng);
    this._updatePosition();
    return this;
  },

  setContent: function (content) {
    this._content = content;
    this._updateContent();
    return this;
  },

  setDirection: function(direction) {
    this._direction = direction;
    this._updatePosition();
    return this;
  },

  getDirection: function() {
    return this._direction || this.options.direction;
  },

  close: function () {
    let map = this._map;

    if (map) {
      if (Leaflet.Browser.touch && !this.options.noHide) {
        Leaflet.DomEvent.off(this._container, 'click', this.close);
        map.off('click', this.close, this);
      }

      map.removeLayer(this);
    }
  },

  updateZIndex: function (zIndex) {
    this._zIndex = zIndex;

    if (this._container && this._zIndex) {
      this._container.style.zIndex = zIndex;
    }
  },

  setOpacity: function (opacity) {
    this.options.opacity = opacity;

    if (this._container) {
      Leaflet.DomUtil.setOpacity(this._container, opacity);
    }
  },

  _initLayout: function () {
    this._container = Leaflet.DomUtil.create('div', 'leaflet-label ' + this.options.className + ' leaflet-zoom-animated');
    this._container.style.position = 'absolute';
    this._container.style.left = 0;
    this._container.style.top = 0;
    this.updateZIndex(this._zIndex);
  },

  _update: function () {
    if (!this._map) {
      return;
    }

    this._container.style.visibility = 'hidden';

    this._updateContent();
    this._updatePosition();

    this._container.style.visibility = '';
  },

  _updateContent: function () {
    if (!this._content || !this._map || this._prevContent === this._content) {
      return;
    }

    while (this._container.hasChildNodes()) {
      this._container.removeChild(this._container.lastChild);
    }

    if (typeof this._content === 'string') {
      this._container.innerHTML = this._content;
    } else {
      this._container.appendChild(this._content);
    }

    this._prevContent = this._content;
    this._labelBox = BboxUtil.getElementBox(this._container);
  },

  _updatePosition: function () {
    if (this._map) {
      let pos = this._map.latLngToLayerPoint(this._latlng);
      this._setPosition(pos);
    }
  },

  _setLeafletClassForDirection: function(el, direction) {
    Leaflet.DomUtil.removeClass(el, 'leaflet-label-right');
    Leaflet.DomUtil.removeClass(el, 'leaflet-label-left');
    Leaflet.DomUtil.removeClass(el, 'leaflet-label-top');
    Leaflet.DomUtil.removeClass(el, 'leaflet-label-bottom');

    Leaflet.DomUtil.addClass(el, 'leaflet-label-' + direction);
  },

  _getSourceOffset: function(pos, direction) {
    if (this._source instanceof Leaflet.Marker) {
      let sourceBbox = BboxUtil.getElementBox(this._source._icon);
      sourceBbox = BboxUtil.positionBox(pos, sourceBbox);

      let labelBox = BboxUtil.positionBox(pos, this._labelBox);

      let sourceWidth = sourceBbox.maxX - sourceBbox.minX;
      let sourceHeight = sourceBbox.maxY - sourceBbox.minY;

      let labelWidth = labelBox.maxX - labelBox.minX;
      let labelHeight = labelBox.maxY - labelBox.minY;

      let centerHorizontalOffset = sourceBbox.minX - pos.x + ( (sourceWidth - labelWidth) / 2 );
      let centerVerticalOffset = sourceBbox.minY - pos.y + ( (sourceHeight - labelHeight) / 2 );

      if (direction === 'right') {
        return Leaflet.point(sourceBbox.maxX - pos.x, centerVerticalOffset);
      }
      if (direction === 'left') {
        return Leaflet.point(sourceBbox.minX - pos.x - labelWidth, centerVerticalOffset);
      }
      if (direction === 'top') {
        return Leaflet.point(centerHorizontalOffset, sourceBbox.minY - pos.y - labelHeight);
      }
      if (direction === 'bottom') {
        return Leaflet.point(centerHorizontalOffset, sourceBbox.maxY - pos.y);
      }
    }
    return Leaflet.point(0,0);
  },

  _setPosition: function (pos) {
    let container = this._container,
      direction = this.getDirection(),
      offset = Leaflet.point(this.options.offset).add(this._getSourceOffset(pos, direction));

    this._setLeafletClassForDirection(container, direction);

    container.style.marginLeft = offset.x + "px";
    container.style.marginTop = offset.y + "px";
    Leaflet.DomUtil.setPosition(container, pos);
  },

  _zoomAnimation: function (opt) {
    let pos = this._map._latLngToNewLayerPoint(this._latlng, opt.zoom, opt.center).round();

    this._setPosition(pos);
  },

  _onViewReset: function (e) {
    /* if map resets hard, we must update the label */
    if (e && e.hard) {
      this._update();
    }
  },

  _initInteraction: function () {
    if (!this.options.clickable) { return; }

    let container = this._container,
      events = ['dblclick', 'mousedown', 'mouseover', 'mouseout', 'contextmenu'];

    Leaflet.DomUtil.addClass(container, 'leaflet-clickable');
    Leaflet.DomEvent.on(container, 'click', this._onMouseClick, this);

    for (let i = 0; i < events.length; i++) {
      Leaflet.DomEvent.on(container, events[i], this._fireMouseEvent, this);
    }
  },

  _removeInteraction: function () {
    if (!this.options.clickable) { return; }

    let container = this._container,
      events = ['dblclick', 'mousedown', 'mouseover', 'mouseout', 'contextmenu'];

    Leaflet.DomUtil.removeClass(container, 'leaflet-clickable');
    Leaflet.DomEvent.off(container, 'click', this._onMouseClick, this);

    for (let i = 0; i < events.length; i++) {
      Leaflet.DomEvent.off(container, events[i], this._fireMouseEvent, this);
    }
  },

  _onMouseClick: function (e) {
    if (this.hasEventListeners(e.type)) {
      Leaflet.DomEvent.stopPropagation(e);
    }

    this.fire(e.type, {
      originalEvent: e
    });
  },

  _fireMouseEvent: function (e) {
    this.fire(e.type, {
      originalEvent: e
    });

    // this line will always be called if marker is in a FeatureGroup
    if (e.type === 'contextmenu' && this.hasEventListeners(e.type)) {
      Leaflet.DomEvent.preventDefault(e);
    }
    if (e.type !== 'mousedown') {
      Leaflet.DomEvent.stopPropagation(e);
    } else {
      Leaflet.DomEvent.preventDefault(e);
    }
  }
});

module.exports.label = function(options, source) {
  return new LeafletLabel(options, source);
};
