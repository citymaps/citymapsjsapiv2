'use strict';

import request from 'superagent';
import urlTemplate from 'url-template';

let LoadingGrid = require('./leaflet/Leaflet.LoadingGrid');
const SUPPORTED_LANGUAGES = ["global","ar","cs","da","de","el","en","es","fi","fr","he","hu","id","it","ja","ko","nl","no","pl","pt","ru","sk","sr","sv","th","tr","vi","zh"];

const devHost  = "raster.citymaps.svc.kube.cm.dev.tripadvisor.com";
const prodHost = "trip-raster.citymaps.io";

const CITYMAPS_DEFAULTS = {
  attributionControl: false,
  citymapsTileLayerHost: 'prod',
  keepBuffer: 1,
  loadingGrid: true,
  language: "global",
  maxZoom: 18,
  minZoom: 3,
  touchZoom: 'center',
  scrollWheelZoom: false,
  updateWhenIdle: false,
  zoomControl: false,
  scaleControl: true,
  scalePosition: 'bottomright',

  /* Do not bounce at max / min zoom */
  bounceAtZoomLimits: false,
  /* Let the user wrap the world, but still block the north and south edges. */
  maxBounds: Leaflet.latLngBounds(Leaflet.latLng(-90, -180000), Leaflet.latLng(90, 180000)),
  /* Jump back to the normal LatLng range when world wrapping. Keeps markers working. */
  worldCopyJump: true,
  /* Hard wall at the north/south edge of the world */
  maxBoundsViscosity:1.0
};

const RASTER_URL_TEMPLATE = "https://{host}/raster/{version}/{language}/{z}/{x}/{y}";
const RASTER_VERSION_URL = "https://{host}/version";
const DEFAULT_RASTER_VERSION = "v4";

let Citymap = Leaflet.Map.extend({

  initialize: function(element, _, options) {
    let all_options = Object.assign({}, Leaflet.Map.prototype.options, CITYMAPS_DEFAULTS, options);
    Leaflet.Map.prototype.initialize.call(this, element, all_options);

    let lang_index = SUPPORTED_LANGUAGES.indexOf(all_options.language);
    if(lang_index === -1) {
      console.error("Citymaps - unsupported language: "+all_options.language+". Defaulting to global");
      lang_index = 0;
    }

    let tileLayerOptions = {
      attribution: '&copy; Citymaps &copy; <a href="http://www.openstreetmap.org/about/" target="_blank" />OpenStreetMap</a>',
      fixZoomAnim: false,
      language: SUPPORTED_LANGUAGES[lang_index],
      maxZoom: all_options.maxZoom,
      updateWhenIdle: this.options.updateWhenIdle,
      keepBuffer: this.options.keepBuffer
    };

    // MAPS-1320
    if (tileLayerOptions.language === "en") {
      tileLayerOptions.attribution += " contributors";
    }

    Leaflet.control.attribution({ prefix: "" }).addTo(this);

    if (this.options.scaleControl) {
      Leaflet.control.scale({
        maxWidth: 200,
        position: this.options.scalePosition,
        updateWhenIdle: true,
      }).addTo(this);
    }

    // add Citymaps layers if no layers were provided in the options.
    if (all_options.layers.length === 0) {
      switch(all_options.citymapsTileLayerHost) {
        case 'dev':
          tileLayerOptions.host = devHost;
          break;
        default:
          tileLayerOptions.host = prodHost;
      }

      if (this.options.loadingGrid) {
        this._loadingGrid = LoadingGrid.loadingGrid({
          zoomAnimation: true
        });
        this.addLayer(this._loadingGrid);
      }

      this.baseLayer = Leaflet.tileLayer('', tileLayerOptions);

      let rasterVersionUrl = RASTER_VERSION_URL.replace('{host}', tileLayerOptions.host);
      request.get(rasterVersionUrl)
        .then(resp => {
          this.baseLayer.options.version = resp.body.raster_tile_version || DEFAULT_RASTER_VERSION;
          this.baseLayer.setUrl(RASTER_URL_TEMPLATE);
        })
        .catch(e => {
          console.warn("Unable to reach raster config file.", e);
          this.baseLayer.options.version = DEFAULT_RASTER_VERSION;
          this.baseLayer.setUrl(RASTER_URL_TEMPLATE);
        });

      this.addLayer(this.baseLayer);

      window.__citymapsCurrentBaseMapURL = tileLayerOptions.host;

      if (this._loadingGrid) {
        this._loadingGrid.setBaseTileLayer(this.baseLayer);
      }

      document.addEventListener("citymaps_bookmarklet_tile_url_update", function(e) {
        console.log(e);
        if (e.host === "dev") {
            this.baseLayer.options.host = devHost;
        } else if (e.host === "prod") {
            this.baseLayer.options.host = prodHost;
        } else {
            this.baseLayer.options.host = e.host;
        }

        window.__citymapsCurrentBaseMapURL = this.baseLayer.options.host;

        this.baseLayer.redraw();
      }.bind(this));
    }
  },

  getBaseLayer: function() {
    return this.baseLayer;
  }
});

module.exports.map = (element, _, options) => {
  return new Citymap(element, _, options);
};
