'use strict';

let map = require('./map');
let AssetUrl = require('./util/AssetUrl');
const VERSION = require('../package.json').version;

if (document && document.head) {
  let link = document.createElement('link');
  link.href = AssetUrl('citymaps.css');
  link.type = "text/css";
  link.rel = "stylesheet";
  document.head.appendChild(link);
} else {
  console.error("Unable to insert Citymaps stylesheet!");
}

Object.assign(Leaflet, require('./leaflet/Leaflet.RelativeAnchorIcon'));


Leaflet.citymaps = module.exports = {
  VERSION: VERSION,
  map: map.map
};
