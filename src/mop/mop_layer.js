'use strict';

let rbush = require('rbush');
let BboxUtil = require('../util/html_bbox_util');

const parentClass = Leaflet.LayerGroup;

const Directions = ["right", "left", "top", "bottom"];

/**
 * This layer group will automatically show and hide each layer's label (if it exists) so that labels do not overlap other labels or layers.
 * 
 * If you change an existing layer, and it does not update the label visibility, you may have to call calculateLabelVisibility directly.
 *  - One known case of this is altering the icon size of an existing marker.
 */
let MopLayer = parentClass.extend({

  options: {
    margin: 0,
    directions: Directions
  },

  initialize: function(options) {
    parentClass.prototype.initialize.call(this);

    Leaflet.setOptions(this, options);
    this._originalLayers = [];
    this._visibleLayers = [];
    this._rbush = rbush();
    this._cachedRelativeLayerBoxes = [];
  },

  addLayer: function(layer) {
    parentClass.prototype.addLayer.call(this, layer);

    this._attachEvents(layer);
    this._originalLayers.push(layer);

    if (this._map) {
      this.calculateLabelVisibility();
    }
  },

  removeLayer: function(layer) {
    parentClass.prototype.removeLayer.call(this,layer);

    this._detachEvents(layer);
    let i = this._originalLayers.indexOf(layer);
    if (i !== -1) { this._originalLayers.splice(i,1); }

    i = this._visibleLayers.indexOf(layer);
    if (i !== -1) { this._visibleLayers.splice(i,1); }

    if (this._map) {
      this.calculateLabelVisibility();
    }
  },

  _attachEvents: function(layer) {
    layer.on('move', this.calculateLabelVisibility, this)
  },

  _detachEvents: function(layer) {
    layer.off('move', this.calculateLabelVisibility, this)
  },

  clearLayers: function() {
    parentClass.prototype.clearLayers.call(this);

    this._rbush = rbush();
    for (let i=0; i<this._originalLayers.length-1; i++) {
      this._detachEvents(this._originalLayers[i]);
    }

    this._originalLayers = [];
    this._visibleLayers  = [];
    this._cachedRelativeLayerBoxes = [];
  },

  onAdd: function (map) {
    this._map = map;

    for (let i=0; i<this._originalLayers.length; i++) {
      map.addLayer(this._originalLayers[i]);
    }

    this._onZoomEnd();
    map.on('zoomend', this._onZoomEnd, this);
  },

  onRemove: function(map) {
    for (let i=0; i<this._originalLayers.length; i++) {
      map.removeLayer(this._originalLayers[i]);
    }
    this._originalLayers = [];

    map.off('zoomend', this._onZoomEnd, this);
    parentClass.prototype.onRemove.call(this, map);
  },

  _getLayerBoxes: function(layer) {
    if (!layer._icon) {
      return [];
    }

    let boxes = this._cachedRelativeLayerBoxes[layer._leaflet_id];
    if (!boxes) {
      let box = BboxUtil.getElementBox(layer._icon);
      boxes = BboxUtil.getRelativeElementBoxes(layer._icon.children, box);
      boxes.push(box);
      this._cachedRelativeLayerBoxes[layer._leaflet_id] = boxes;
    }
    boxes = BboxUtil.positionBoxes(this._map.latLngToLayerPoint(layer.getLatLng()),boxes);

    for (let i = 0; i < boxes.length; i++) {
      boxes[i]._leaflet_id = layer._leaflet_id;
    }

    return boxes;
  },

  _getLayerLabelBoxes: function(layer) {
    layer.showLabel();

    let box = BboxUtil.getElementBox(layer.label._container);
    let boxes = BboxUtil.getRelativeElementBoxes(layer.label._container.children, box);
    boxes.push(box);
    boxes = BboxUtil.positionBoxes(this._map.latLngToLayerPoint(layer.getLatLng()),boxes);

    return boxes;
  },

  _addLayerBoxesToRBush: function(layer) {
    let bush = this._rbush;
    let layerBoxes = this._getLayerBoxes(layer);
    bush.load(layerBoxes);
  },

  _maybeAddLabelToRBush: function(layer) {

    if (!layer.label) {
      return false;
    }

    let bush = this._rbush;

    let layerLabelBoxes = this._getLayerLabelBoxes(layer);

    let collision = false;
    for (let i = 0; i < layerLabelBoxes.length && !collision; i++) {
      let collidingBoxes = bush.search(layerLabelBoxes[i]);
      for(let collidingBoxIndex = 0; collidingBoxIndex < collidingBoxes.length && !collision; collidingBoxIndex++) {
        collision = collidingBoxes[collidingBoxIndex]._leaflet_id === undefined || collidingBoxes[collidingBoxIndex]._leaflet_id !== layer._leaflet_id;
      }
    }

    if (!collision) {
      layer.showLabel();
      this._visibleLayers.push(layer);
      bush.load(layerLabelBoxes);
      return true;
    } else {
      layer.hideLabel();
      return false;
    }
  },

  calculateLabelVisibility: function() {
    this._visibleLayers = [];
    this._rbush = rbush();

    for (let i = 0; i < this._originalLayers.length; i++) {
      this._addLayerBoxesToRBush(this._originalLayers[i]);
    }

    for (let i = 0; i < this._originalLayers.length; i++) {
      let added = false;

      let layer = this._originalLayers[i];

      if (!layer.label) {
        continue;
      }
      // Attempt to add it with the existing direction.
      if (!this._maybeAddLabelToRBush(this._originalLayers[i])) {
        let originalDirection = layer.label.getDirection();
        let directions = this.options.directions;

        for(let dirIndex = 0; dirIndex < directions.length && !added; dirIndex++) {

          // We already tried the original direction, skip it here.
          if (originalDirection !== directions[dirIndex]) {
            this._originalLayers[i].updateLabelDirection(directions[dirIndex]);
            this._originalLayers[i].showLabel();
            added = this._maybeAddLabelToRBush(this._originalLayers[i]);
          }
        }
      }
    }
  },

  _onZoomEnd: function() {
    this.calculateLabelVisibility();
  }
});

module.exports.richPinLayer = function(options) {
  return new MopLayer(options);
};
